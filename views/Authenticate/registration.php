<?php
include_once('../../vendor/autoload.php');
session_start();
use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;

$auth= new Auth();
$status= $auth->prepare($_POST)->is_exist();

if($status){
    Message::message("<div class=\"alert alert-danger\">
  <strong>Success!</strong> Email you entered are already taken, plz try to login
</div>");
    Utility::redirect('../../index.php');

}else{
    $user= new User();
    $user->prepare($_POST)->store();
}

